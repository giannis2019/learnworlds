<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Model;


class MyController extends Controller {

    private $router;
    
    public function __construct(RouterInterface $router){
        $this->router = $router;     
    }
    
    /**
     * @Route("/", name="homepage",defaults={"_locale": "gr"})
     */     
    public function indexAction(Request $request) {
        
        return $this->render("common/home.html.twig",array(
                            'fullname' => 'Enter your full name',
                            'email' => 'Enter your email',
                            'hasaccept' => 'Accept to receive newletter',
                            'formaction' => $this->router->generate('addUser')));        
    }
    
    /**
     * @Route({"gr": "/addUser"}, name="addUser")
     */     
    public function addUser(Request $request) {
        
        //add user in database from modal form
        $db = $this->getDoctrine()->getManager();
        $user = new Model($db);
        
        $userinfo = array();
        $info['success'] = false;
        $hasaccept = 0;
        
        $userinfo['fullname'] = $request->request->get('fullname');
        $userinfo['email'] = $request->request->get('email');
        if ($request->request->get('hasaccept') == 1) {
            $hasaccept = 1;
        }        
        $userinfo['hasaccept'] = $hasaccept;
        
        if ($user->addUser($userinfo)) {
            $info['success'] = true;
            unset($user);
            
           $this->addMailchimp($userinfo);
        }
        
        return new Response(json_encode($info));
    }
    
    
    /**
     * @Route({"gr": "/getUsers"}, name="getUsers")
     */     
    public function getUsers(Request $request) {
        //get list from database
        $db = $this->getDoctrine()->getManager();
        $users = array();
        $user = new Model($db);
        
        $users = $user->getUsers();
        
        return $this->render("common/listusers.html.twig",array(
                            'fullname' => 'Full Name',
                            'email' => 'Email',
                            'dateofreg' => 'DateTime Of Registration',
                            'hasaccept' => 'Accept Newsletter',
                            'userlist' => $users));   
    }
    
    protected function addMailchimp($info) {
        
        //mailchimp apikey 44f1e538245ef5538d6c825828b5ec8b-us20
			$apiKey = '44f1e538245ef5538d6c825828b5ec8b-us20';
			$listId = 'fb6473993d' ;
		            
            $fullname = explode (' ', $info['fullname']);
            $fname = $fullname[0];
            $lname = $fullname[1];
            
            $postData = array(
                "email_address" => $info['email'], 
                "status" => 'subscribed', 
				'merge_fields' => array(
					'FNAME'     => $fname,
					'LNAME'     => $lname)
            );
		
            $ch = curl_init('https://us20.api.mailchimp.com/3.0/lists/'.$listId.'/members/');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Authorization: apikey '. $apiKey,
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($postData)
            ));
            
            $response = curl_exec($ch);

			return true;        
    }
}
?>