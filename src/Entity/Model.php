<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class Model {

private $em;

	public function __construct($db) {				
		$this->em = $db;
	}
	
	public function __destruct() {				
		$this->em->getConnection()->close();
	}
	
	public function addUser($info) {
		
		$conn = $this->em->getConnection();
		$params = array();
		$issuccess = false;
		
		$sql ='INSERT INTO users (fullname,email,dateofreg,hasaccept) VALUES (:fullname,:email,NOW(),:hasaccept)';
		
		$stmt = $conn->prepare($sql);
		
		$params['fullname'] = $info['fullname'];
		$params['email'] = $info['email'];
		$params['hasaccept'] = $info['hasaccept'];
		
		if ($stmt->execute($params)) {
			$issuccess = true;
		}
		
		return $issuccess;
	}
	
	public function getUsers() {
		
		$conn = $this->em->getConnection();
		$users = array();
		
		$sql ="SELECT fullname,email,hasaccept,DATE_FORMAT(dateofreg, '%d/%m/%Y') AS date_registration,TIME_FORMAT(dateofreg, '%H:%i') AS time_registration FROM users";
		
		$stmt = $conn->prepare($sql);
		$stmt->execute();
		$users = $stmt->fetchAll();
		
		return $users;		
	}
}