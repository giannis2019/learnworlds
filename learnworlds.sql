/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : learnworlds

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-04-25 15:31:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `userid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `dateofreg` datetime DEFAULT NULL,
  `hasaccept` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'giannis tsiagkas', 'i.tsiagkas@concise.gr', '2019-04-25 13:01:18', '1');
INSERT INTO `users` VALUES ('2', 'giannis tsiagkas', 'i.tsiagkas@concise.gr', '2019-04-25 13:38:41', '0');
INSERT INTO `users` VALUES ('3', 'giannis tsiagkas', 'i.tsiagkas@concise.gr', '2019-04-25 13:44:06', '0');
INSERT INTO `users` VALUES ('4', 'giannis tsiagkas', 'i.tsiagkas@concise.gr', '2019-04-25 13:44:44', '1');
INSERT INTO `users` VALUES ('5', 'giannis tsiagkas', 'i.tsiagkas@concise.gr', '2019-04-25 13:45:11', '1');
INSERT INTO `users` VALUES ('6', 'giannis tsiagkas', 'info@concise.gr', '2019-04-25 13:47:57', '0');
INSERT INTO `users` VALUES ('7', 'giannis tsiagkas', 'aniron_morgan@hotmail.com', '2019-04-25 13:48:20', '0');
INSERT INTO `users` VALUES ('8', 'giannis tsiagkas', 'i.tsiagkas@gmail.com', '2019-04-25 13:49:35', '0');
INSERT INTO `users` VALUES ('9', 'sofia dermati', 'aniron_morgan@hotmail.com', '2019-04-25 13:53:25', '1');
INSERT INTO `users` VALUES ('10', 'giannis tsiagkas', 'i.tsiagkas@concise.gr', '2019-04-25 13:54:33', '1');
INSERT INTO `users` VALUES ('11', 'giannis tsiagkas', 'info@concise.gr', '2019-04-25 13:54:53', '1');
INSERT INTO `users` VALUES ('12', 'giannis tsiagkas', 'info@ncise.gr', '2019-04-25 13:56:16', '1');
INSERT INTO `users` VALUES ('13', 'tasos', 'ingo@kati.gr', '2019-04-25 13:56:38', '1');
INSERT INTO `users` VALUES ('14', 'tasos kapoios', 'tasos@kati.gr', '2019-04-25 13:57:17', '1');
INSERT INTO `users` VALUES ('15', 'kostas paapdopoulos', 'kostas@kapoukati.com', '2019-04-25 14:01:47', '1');
